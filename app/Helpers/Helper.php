<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Validator;

// Add this in `config\app.php`
class Helper
{
    // Takes in a string and converts it to a slug
    public static function createSlug($str, $delimiter = '-')
    {
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    /**
     * Check is the file is a valid image file or not.
     */
    public static function isImage($file)
    {
        $fileArray = array('image' => $file);
        $rules = array('image' => 'mimes:jpeg,jpg,png,gif|required|max:10000');
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails())
            return false;
        return true;
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrsstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
