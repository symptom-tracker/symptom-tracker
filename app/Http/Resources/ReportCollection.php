<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($report) {
                return [
                    'id' => $report->id,
                    'lat' => $report->lat,
                    'lng' => $report->lng,
                    'symptoms' => new SymptomCollection($report->symptoms),
                    'created_at' => $report->created_at,
                ];
            }),
        ];
    }
}
