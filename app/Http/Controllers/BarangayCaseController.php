<?php

namespace App\Http\Controllers;

use App\Barangay;
use App\BarangayCase;
use Illuminate\Http\Request;

class BarangayCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'barangay' => 'integer|required|max:191',
            'date' => 'date|required|max:191',
            'case_statuses.*' => 'integer|nullable|max:191',
        ]);

        if (!Barangay::find(request('barangay'))) {
            return back()->with('warning', 'Invalid barangay ID.');
        }

        $caseStatuses = request('case_statuses');

        foreach (array_keys($caseStatuses) as $caseStatusID) {

            $barangayCase = BarangayCase::where('barangay_id', request('barangay'))
                ->where('case_status_id', $caseStatusID)
                ->where('date', request('date'))
                ->first();

            if (!isset($barangayCase)) {
                $barangayCase = new BarangayCase();
            }

            $barangayCase->barangay_id = request('barangay');
            $barangayCase->case_status_id = $caseStatusID;
            $barangayCase->number = $caseStatuses[$caseStatusID];
            $barangayCase->date = request('date');

            if (!$barangayCase->save()) {
                return back()->with('danger', 'Error saving barangay case.');
            }
        }

        $city = Barangay::find(request('barangay'))->city;

        return redirect(route('admin.cities.show', ['city' => $city->id]))->with('success', 'Added cases.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BarangayCase  $barangayCase
     * @return \Illuminate\Http\Response
     */
    public function show(BarangayCase $barangayCase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BarangayCase  $barangayCase
     * @return \Illuminate\Http\Response
     */
    public function edit(BarangayCase $barangayCase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BarangayCase  $barangayCase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BarangayCase $barangayCase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BarangayCase  $barangayCase
     * @return \Illuminate\Http\Response
     */
    public function destroy(BarangayCase $barangayCase)
    {
        //
    }
}
