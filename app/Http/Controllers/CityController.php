<?php

namespace App\Http\Controllers;

use App\City;
use App\Province;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::all();
        $provinces = Province::all();

        return view('admin.cities.index', compact('cities', 'provinces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'province' => 'integer|required|max:191',
        ]);

        $city = new City();

        if (!Province::find(request('province'))) {
            return back()->with('warning', 'Invalid province ID.');
        }

        $city->province_id = request('province');
        $city->name = request('name');

        if ($city->save()) {
            return redirect(route('admin.cities.index'))->with('success', 'Added new city.');
        }
        return back()->with('danger', 'Erorr adding new city.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return view('admin.cities.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        $provinces = Province::all();

        return view('admin.cities.edit', compact('city', 'provinces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $validatedData = $request->validate([
            'name' => 'string|required|max:191',
            'province' => 'integer|required|max:191',
        ]);

        if (!Province::find(request('province'))) {
            return back()->with('warning', 'Invalid province ID.');
        }

        $city->province_id = request('province');
        $city->name = request('name');

        if ($city->save()) {
            return redirect(route('admin.cities.index'))->with('success', 'Updated city.');
        }
        return back()->with('danger', 'Erorr updating city.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        foreach ($city->barangays as $barangays) {
            if (!$barangays->delete()) {
                return back()->with('danger', 'Error deleting barangay of city.');
            }
        }

        if ($city->delete()) {
            return back()->with('success', 'Deleted city');
        }
        return back()->with('danger', 'Error deleting city.');
    }
}
