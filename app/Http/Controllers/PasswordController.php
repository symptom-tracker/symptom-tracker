<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PasswordController extends Controller
{

    public function createPassword(String $token)
    {
        return view('auth.password-verification', compact('token'));
    }

    /**
     * Register password for a newly created admin in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePassword(Request $request)
    {
        Auth::logout();

        $validatedData = $request->validate([
            'email' => 'required|string|email|max:90',
            'password' => 'required|confirmed|min:6',
        ]);

        $token = $request->token;
        $email = $request->email;
        $password_reset = DB::table('password_resets')->where('email', $email)->first();

        if (!isset($password_reset))
            return back()->with('danger', 'Invalid request');

        // Check if email is the same as the one in password_resets table
        if ($email != $password_reset->email)
            return back()->with('danger', 'Invalid email');

        // CHeck if the token in the url is exists and it not yet used
        if (Hash::check($token, $password_reset->token) != 1)
            return back()->with('danger', 'Invalid token.');

        // Check if the passwords match
        if ($request->password != $request->password_confirmation)
            return back()->with('danger', 'Passwords do not match.');

        $admin = User::where('email', $email)->first();
        $admin->password = Hash::make($request->password);
        $admin->email_verified_at = date('Y-m-d H:i:s');

        if ($admin->save()) {
            return redirect('login')->with('success', 'Successfully created a password for the admin. ');
        }
        return back()->with('danger', 'Error in creating password for the admin.');
    }
}
