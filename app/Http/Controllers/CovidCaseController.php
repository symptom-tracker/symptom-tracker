<?php

namespace App\Http\Controllers;

use App\CaseStatus;
use App\CovidCase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CovidCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cases = CovidCase::latest()->paginate(50);
        $caseStatuses = CaseStatus::all();

        return view('admin.cases.index', compact('cases', 'caseStatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'case_status' => 'integer|required',
            'case_number' => 'string|nullable',
            'lat' => 'string|nullable|max:100',
            'lng' => 'string|nullable|max:100',
            'barangay' => 'string|nullable',
            'city' => 'string|nullable',
            'province' => 'string|nullable',
            'age' => 'integer|nullable|min:0|max:116',
            'sex' => 'integer|nullable|digits:1',
        ]);

        $case = new CovidCase();

        if (!CaseStatus::find(request('case_status'))) {
            return back()->with('warning', 'Invalid case status ID.');
        }

        $case->case_status_id = request('case_status');
        $case->added_by = Auth::id();
        $case->case_number = request('case_number');
        $case->lat = request('lat');
        $case->lng = request('lng');
        $case->barangay = request('barangay');
        $case->city = request('city');
        $case->province = request('province');
        $case->age = request('age');
        $case->sex = request('sex');

        if ($case->save()) {
            return back()->with('success', 'Added case.');
        }
        return back()->with('danger', 'Error adding case.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CovidCase  $case
     * @return \Illuminate\Http\Response
     */
    public function show(CovidCase $case)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CovidCase  $case
     * @return \Illuminate\Http\Response
     */
    public function edit(CovidCase $case)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CovidCase  $case
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CovidCase $case)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CovidCase  $case
     * @return \Illuminate\Http\Response
     */
    public function destroy(CovidCase $case)
    {
        if ($case->delete()) {
            return back()->with('success', 'Deleted case');
        }
        return back()->with('danger', 'Error deleting case.');
    }
}
