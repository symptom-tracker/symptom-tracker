<?php

namespace App\Http\Controllers;

use App\Report;
use App\Symptom;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $symptoms = Symptom::all();
        $allReports = Report::all();
        $reports = Report::all()->reverse()->take(5);
        $users = User::all();

        return view('admin.dashboard', compact('symptoms', 'reports', 'users', 'allReports'));
    }
}
