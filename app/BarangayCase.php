<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangayCase extends Model
{
    public function barangay()
    {
        return $this->belongsTo('App\Barangay');
    }

    public function caseStatus()
    {
        return $this->belongsTo('App\CaseStatus');
    }
}
