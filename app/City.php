<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function barangays()
    {
        return $this->hasMany('App\Barangay');
    }
}
