window.onscroll = function () {
  var currScrollPos = window.pageYOffset;
  var appHeader = document.getElementById("app-header");
  var appNavbar = document.getElementById("app-navbar");
  var appNavbarTransparent = document.getElementById("app-navbar-transparent");
  var appNavbarMobile = document.getElementById("app-navbar-transparent-m");
  var offsetHeader = appHeader.offsetHeight;
  var offsetNavbar = appNavbar.offsetHeight;
  var offsetNavbarTransparent = appNavbarTransparent.offsetHeight;
  var offsetNavbarMobile = appNavbarMobile.offsetHeight;

  // Show navbar on first scroll
  if (currScrollPos > offsetHeader) {
    appNavbar.classList.remove('d-none');
  }

  // Toggle app header section
  if (currScrollPos <= offsetHeader / 3) {
    // alert(offsetHeader)
    appHeader.style.top = "0";
  } else {
    appHeader.style.top = "-" + offsetHeader + "px";
  }

  // Toggle top navbar (transparent bg)
  if (currScrollPos > 0) {
    appNavbarTransparent.style.top = "-" + offsetNavbarTransparent + "px";
    appNavbarMobile.style.top = "-" + offsetNavbarMobile + "px";
  } else {
    appNavbarTransparent.style.top = "0";
    appNavbarMobile.style.top = "0";
  }

  // Toggle top navbar (opaque bg)
  if (currScrollPos <= offsetHeader / 3) {
    appNavbar.style.top = "-" + offsetNavbar + "px";
  } else {
    appNavbar.style.top = "0";
  }
}
