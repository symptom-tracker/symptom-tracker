@extends('layouts.app.index')

@section('styles')
  <style>
    html {
      min-height:: 100%;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      background: #70bg32;
      background-repeat:no-repeat;
      background: -webkit-linear-gradient(180deg, rgba(0, 219, 182, 0.75) 0%, rgba(19, 134, 159, 0.75) 100%) fixed;
      background: -moz-linear-gradient(180deg, rgba(0, 219, 182, 0.75) 0%, rgba(19, 134, 159, 0.75) 100%) fixed;
      background: -ms-linear-gradient(180deg, rgba(0, 219, 182, 0.75) 0%, rgba(19, 134, 159, 0.75) 100%) fixed;
      background: -o-linear-gradient(180deg, rgba(0, 219, 182, 0.75) 0%, rgba(19, 134, 159, 0.75) 100%) fixed;
      background: linear-gradient(180deg, rgba(0, 219, 182, 0.75) 0%, rgba(19, 134, 159, 0.75) 100%) fixed; }
  </style>
@endsection

@section('content')

  {{-- Load Facebook SDK for JavaScript --}}
  <div id="fb-root"></div>

  {{-- Your customer chat code --}}
  <div class="fb-customerchat"
    attribution=setup_tool
    page_id="108784544101962"
    theme_color="#00DBB6"
    logged_in_greeting="Hi 😊 Thanks for answering! Do you have any questions, comments, or suggestions?"
    logged_out_greeting="Hi 😊 Thanks for answering! Do you have any questions, comments, or suggestions?">
  </div>

  <div class="container">

    <card-basic class="mt-4 text-center rounded shadow border-0">
      <img class="img-fluid" src="{{ asset('images/checkmark.png') }}"/>
      <h1 class="font-gotham-bold text-aqua text-uppercase">{{ __('Thank you!') }}</h1>
      <p>
         {{ __('Your input helps speed up the progress to beat COVID-19') }}
      </p>
      <p>
         {{ __('We learn more about this virus every day you come back.') }} 
         {{ __('This helps us find patterns that will help our health professionals') }}
      </p>

      <div class="d-flex flex-wrap justify-content-center mt-4">
        <a href="{{ route('map') }}" class="btn-green-outlined p-3 mx-2 rounded font-gotham-bold text-uppercase text-green text-center">
          {{ __('Check out the map') }}
        </a>
        <a href="{{ route('survey') }}" class="btn-green p-3 mx-2 rounded font-gotham-bold text-uppercase text-center">
         {{ __('Answer for another person') }}
        </a>
      </div>

    </card-basic>

    <card-basic class="mt-4 text-center rounded shadow border-0">
      <p>{{ __('In case you need further medical assistance, here are some online groups that you may try to reach:') }}</p>
      <div class="d-flex justify-content-center">
        <ul class="text-left m-0">
          <li>
            <a href=" https://www.konsulta.md" target="_blank">KonsultaMD</a>
          </li>
          <li>
            <a href="https://www.facebook.com/COVIDAskForce" target="_blank">Lung Center COVID Ask Force</a>
          </li>
          <li>
            <a href="https://www.medifi.com" target="_blank">Medifi</a>
          </li>
          <li>
            <a href="https://www.telemedph.com" target="_blank">Telemed Philippines</a>
          </li>
        </ul>
      </div>
    </card-basic>

    
    <card-basic class="mt-4 text-center rounded shadow border-0">
      <p>{{ __('Covid-19 Hotlines') }}</p>
      <div class="d-flex justify-content-center">
        <ul class="text-left m-0">
          <li>
            DOH COVID-19 emergency hotlines
          </li>
          <ul class="text-left m-0">
            <li>
              <a href="+630289426843" target="_blank">02-894-COVID (02-894-26843)</a>
            </li>
          </ul>
          <li>
            National Emergency Hotline of the Department of Interior and Local Government (DILG)
          </li>
          <ul class="text-left m-0">
            <li>
              <a href="+63021555" target="_blank">1555</a>
            </li>
          </ul>
        </ul>
      </div>
    </card-basic>

    <card-basic class="mt-4 text-center rounded shadow border-0">
      {{ __('Share this with your friends and family!') }}
      <div class="d-flex justify-content-center mt-4">
        <button onclick="share()" type="button" class="btn btn-green p-3 rounded font-gotham-bold text-uppercase text-center">{{ __('Share') }}</button>
      </div>
    </card-basic>

  </div>
@endsection

@section('scripts')
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v6.0'
      });
    };
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
@endsection
