@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="bg-white w-100 p-2 text-center rounded shadow">
          {{-- Desktop --}}
          <div class="d-none d-md-block">
            <h3 class="font-gotham-bold my-2 spaced-letters text-aqua">FREQUENTLY ASKED QUESTIONS</h3>
          </div>
          {{-- Mobile --}}
          <div class="d-md-none">
            <h3 class="font-gotham-bold my-2 text-aqua">FREQUENTLY ASKED QUESTIONS</h3>
          </div>
        </div>

        <card-basic class="mt-4 p-1 p-md-4 rounded shadow border-0">
          <h6 class="text-muted"><i>Last Updated: March 29, 2020</i></h6>

          <div class="jumbotron bg-light mt-4 py-4">
            <h3 class="font-gotham-bold text-aqua">Contents</h3>

            {{-- Table of contents --}}
            <ul class="list-group list-group-flush">
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q1" class="font-gotham text-blue">
                  1. Who is behind this app? Why are they doing this?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q2" class="font-gotham text-blue">
                  2. Who will use the information collected?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q3" class="font-gotham text-blue">
                  3. Other than public authorities like DOH, DILG and LGU officials, who else will use the data uploaded through this app?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q4" class="font-gotham text-blue">
                  4. Is this a research study?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q5" class="font-gotham text-blue">
                  5. Is there an informed consent?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q6" class="font-gotham text-blue">
                  6. What guarantee do I have that the data that I upload are not misused? Is De La Salle University willing to vouch for the data that are being collected?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q7" class="font-gotham text-blue">
                  7. What if other people to whom you will give the data will misuse the information collected?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q8" class="font-gotham text-blue">
                  8. Who developed the app? What tools were used?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q9" class="font-gotham text-blue">
                  9. I think I can help with the app, is there a way that I can help?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q10" class="font-gotham text-blue">
                  10. Will the app show my exact location? I see a red pin right at my exact house/location.
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q11" class="font-gotham text-blue">
                  11. Why does it show an inaccurate location?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q12" class="font-gotham text-blue">
                  12. If my location is moved and if GPS readings are sometimes inaccurate, will that not cause problems?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q13" class="font-gotham text-blue">
                  13. What do I do if my symptoms change after a few days?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q14" class="font-gotham text-blue">
                  14. Why do I get this error: Access denied. ?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q15" class="font-gotham text-blue">
                  15. Why do I get this error: Service denied. ?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q16" class="font-gotham text-blue">
                  16. Why does it say RED: symptoms + fever, while fever is one of the symptoms?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q17" class="font-gotham text-blue">
                  17. How do I know if my response was included?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q18" class="font-gotham text-blue">
                  18. Is this app DOH approved?
                </link-scroll>
              </li>
              <li class="list-group-item text-light border-0 bg-light">
                <link-scroll href="#q19" class="font-gotham text-blue">
                  19. What difference does it make if all I will upload is “None of the above”?
                </link-scroll>
              </li>
            </ul>
          </div>

          {{-- Policies section --}}
          @component('components.privacy-policy')
            @slot('id', 'q1')
            @slot('title', '1. Who is behind this app? Why are they doing this?')
            @slot('content')
              <p class="text-justify">
                The project team is led by Dr. Arnulfo Azcarraga, Mr. Jay Calleja, and Mr. Cyrus Vatandoost Kakhki.  You may reach us at covid.symtracker@gmail.com.  In this extraordinary time, the project is an attempt to provide the public a crowdsourcing platform where they can share their health status and see from a particular vantage point the state of health of their community amidst the threat of COVID-19. 
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q2')
            @slot('title', '2. Who will use the information collected?')
            @slot('content')
              <p class="text-justify">
              The sole purpose of the submitted information is to generate a crowdsourced map accessible to the public.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q3')
            @slot('title', '3. Other than public authorities like DOH, DILG and LGU officials, who else will use the data uploaded through this app?')
            @slot('content')
              <p class="text-justify">
                The map may be of use to health care professionals in the fields of public health and epidemiology.  Households and community leaders may also refer to the map as a possible source of important information in this time of enhanced community quarantine in various parts of the country. 
              </p>
            @endslot
          @endcomponent
          @component('components.privacy-policy')
            @slot('id', 'q4')
            @slot('title', '4. Is this a research study?')
            @slot('content')
              <p class="text-justify">
              This is not a research study, nor will the data be used for research. We are collecting symptom data using the crowdsourcing approach because we have the skills and the technology to do it, and we think that the information will help our authorities in leading us through this pandemic. This is just our little contribution to the much bigger effort mounted by our officials and the other leaders of the country. 
              </p>
            @endslot
          @endcomponent
          @component('components.privacy-policy')
            @slot('id', 'q5')
            @slot('title', '5. Is there an informed consent?')
            @slot('content')
              <p class="text-justify">
                We took care in having a fairly detailed introduction to all our invitations to the use of the app. Versions in English and Tagalog are used. And users who click on our link will have to agree to share their location and to click on the symptoms and press “SUBMIT”, before we get access to their data. In a way, these are supposed to take the place of “informed consent”.  This is not much different from people who are using Grab or Waze who also are sharing their GPS coordinates, and provide needed information like origin and destination.
              </p>
            @endslot
          @endcomponent
          @component('components.privacy-policy')
            @slot('id', 'q6')
            @slot('title', '6. What guarantee do I have that the data that I upload are not misused? Is De La Salle University willing to vouch for the data that are being collected?')
            @slot('content')
              <p class="text-justify">
                We are giving you our names, and many others are helping us. You also have our contact point. You can check us out; that is probably what you ought to do if you have doubts so you can verify whether the information we gave about ourselves are true and accurate. In the end, there is no real guarantee for something like this. But we do hope that you will trust us. Having said that, we need to clarify that we do not represent our university, nor is this project an official project of the University. This is done in our personal capacities as Filipino citizens. In the coming days, we will try to get official endorsements from authorities. This might address some concerns that you may have which are understandable.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q7')
            @slot('title', '7. What if other people to whom you will give the data will misuse the information collected?')
            @slot('content')
              <p class="text-justify">
                Our data is immediately sent to the “cloud” for safe storage and easy access. We do not maintain printed copies of the data, nor any digital files in our own computers that may carry the risk of being stolen or inadvertently shared with other people. And we only keep the scrambled GPS coordinates, so even our database does not have your exact household locations. And we cannot control how much scrambling of the location will be done, and by how many meters the coordinates will be moved. This way, the exact household cannot really be deduced from the collected GPS coordinates. And note that not even DOH nor Baranggay Captains will get access to the database containing the symptoms data and the GPS coordinates that were collected. All that they can see are the very same colored pin locations that just about anyone else can see.
              </p>
            @endslot
          @endcomponent
          @component('components.privacy-policy')
            @slot('id', 'q8')
            @slot('title', '8. Who developed the app? What tools were used?')
            @slot('content')
              <p class="text-justify">
                Students from DLSU. The main framework used for the site is Laravel, which uses the PHP Language. Google Maps is used to show the survey results on a map. It’s being hosted on DigitalOcean
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q9')
            @slot('title', '9. I think I can help with the app, is there a way that I can help?')
            @slot('content')
              <p class="text-justify">
                We need all kinds of help, not just technical assistance. Please visit this website about what else needs to be done. Or contact the project team: covid.symtracker@gmail.com
              </p>
            @endslot
          @endcomponent

          
          @component('components.privacy-policy')
            @slot('id', 'q10')
            @slot('title', '10. Will the app show my exact location? I see a red pin right at my exact house/location.')
            @slot('content')
              <p class="text-justify">
                No. To protect your privacy, your location will be offset randomly to a nearby location. You get to see the red pin initially pointed at your house/location to confirm that we have been able to get your GPS coordinates, but the Summary Map will not have the exact coordinates, as explained above. We are currently re-thinking this part of the app. It was meant initially to assure the users that the app can accurately pick up the data. But we always get this kind of concern, and so maybe, we can remove this feature in future versions of the app.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q11')
            @slot('title', '11. Why does it show an inaccurate location?')
            @slot('content')
              <p class="text-justify">
                  This may be the case if you are accessing the app using your laptop. Not all laptops are equipped to detect and send your GPS coordinates. Your location is then determined by the GPS data provided by your ISP through your Wi-Fi connection. Sometimes, this yields inaccurate location.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q12')
            @slot('title', '12. If my location is moved and if GPS readings are sometimes inaccurate, will that not cause problems?')
            @slot('content')
              <p class="text-justify">
                  That is OK. Because in any case, we are after the overall change and spread of symptoms in communities, and so the exact location is not all that important.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q13')
            @slot('title', '13. What do I do if my symptoms change after a few days?')
            @slot('content')
              <p class="text-justify">
                  You are supposed to submit another set of responses once your symptoms change.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q14')
            @slot('title', '14. Why do I get this error: Access denied. ?')
            @slot('content')
              <p class="text-justify">
                  This was an issue during the early launch of the app. It has now been resolved. If not please email us.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q15')
            @slot('title', '15. Why do I get this error: Access denied. ?')
            @slot('content')
              <p class="text-justify">
                  This was an issue during the early launch of the app. It has now been resolved. If not please email us.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q16')
            @slot('title', '16. Why does it say RED: symptoms + fever, while fever is one of the symptoms?')
            @slot('content')
              <p class="text-justify">
                  It means fever is among the symptoms experienced by the person (positive on fever).
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q17')
            @slot('title', '17. How do I know if my response was included?')
            @slot('content')
              <p class="text-justify">
                  After submitting your response, you should find a “Thank You!” message on the app.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q18')
            @slot('title', '18. Is this app DOH approved?')
            @slot('content')
              <p class="text-justify">
                No. This is an initiative of private individuals to produce a highly visual and easy to understand map of COVID-19 symptoms through crowdsourced information. We are not diagnosing patients, we are just collecting information from people who are willing to share their current status - on a purely voluntary basis. We are inviting DOH and the LGU officials to use the information in whatever way these  are meaningful to them. They can access the maps on their own. For example, they can prepare for outbreaks in specific locations and can start mobilizing equipment and personnel early based on the information they see on the map.
              </p>
            @endslot
          @endcomponent

          @component('components.privacy-policy')
            @slot('id', 'q19')
            @slot('title', '19. What difference does it make if all I will upload is “None of the above”?')
            @slot('content')
              <p class="text-justify">
                It is important to get baseline information as to when symptoms, as a community, are starting to appear. So when we see mostly green (no symptoms) on a given day, and more and more yellow and red in the coming days, we will be able to track when the symptoms have begun to appear.
              </p>
            @endslot
          @endcomponent

        </card-basic>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
@endsection
