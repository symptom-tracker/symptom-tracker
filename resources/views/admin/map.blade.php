@extends('layouts.admin.index')

@section('styles')
@endsection

@section('content')
  <div class="container-fluid">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Map')
    @endcomponent

    <div class="row">

      <div class="col-md-8">
        <card-basic title="Reports" class="mt-4">
          <template v-slot:image>
            <div id="map" class="w-100" style="height: 512px;"></div>
          </template>
        </card-basic>
      </div>

      <div class="col-md-4">

        <card-basic title="Filter" class="mt-4 border-0 rounded shadow">

          @foreach(App\Symptom::all() as $symptom)
            <div class="custom-control custom-switch mt-2">
              <input
                id="symptomSwitch{{ $symptom->id }}"
                class="custom-control-input"
                type="checkbox"
                onchange="symptomSwitch(this.checked, {{ $symptom->id }})" checked>
              <label class="custom-control-label" for="symptomSwitch{{ $symptom->id }}">
                @if($symptom->name == 'Fever')
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cf44336%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Red Pin">
                @else
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cffee58%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Yellow Pin">
                @endif
                {{ __($symptom->name) }}
              </label>
            </div>
          @endforeach

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="greenMarkerSwitch" checked onchange="greenMarkerSwitch(this.checked)">
            <label class="custom-control-label" for="greenMarkerSwitch">
              <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5C4caf50%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Green Pin">
              {{ __('No symptoms') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="heatMapSwitch" onchange="heatMapSwitch(this.checked)">
            <label class="custom-control-label" for="heatMapSwitch">
              {{ __('Heatmap') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="markerClusterSwitch" onchange="markerClusterSwitch(this.checked)">
            <label class="custom-control-label" for="markerClusterSwitch">
              {{ __('Cluster markers') }}
            </label>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateFrom" class="col-sm-3 col-form-label">From</label>
            <div class="col-sm-9">
              <input type="date" class="form-control" id="inputDateFrom" onchange="refreshDate()">
            </div>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateTo" class="col-sm-3 col-form-label">To</label>
            <div class="col-sm-9">
              <input type="date" class="form-control" id="inputDateTo" onchange="refreshDate()">
            </div>
          </div>

        </card-basic>
      </div>

    </div>

  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/map.js') }}" defer></script>
  <script type="text/javascript" defer>
    // gets all reports from api
    getDataFromAPI = async (url) => {
      const baseURL = window.location.origin;
      const response = await fetch(baseURL + url);
      const json = await response.json();
      return json.data;
    }

    doesMapExist = () => {
      if (document.getElementById("map")) {
        initMap();
      }
      else {
        // TODO: sometimes the google maps api will call initMap() when 'map' hasn't loaded yet and it returns an error
        setTimeout(function () {
          doesMapExist();
        }, 1000);
      }
    }
  </script>
  <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('settings.google_maps_api') }}&libraries=visualization&callback=doesMapExist"></script>
@endsection
