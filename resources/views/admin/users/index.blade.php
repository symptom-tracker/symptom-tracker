@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Users')
    @endcomponent

    @component('components.cards.users')
      @slot('users', $users)
    @endcomponent

  </div>
@endsection
