@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Cases')
    @endcomponent

    @component('components.cards.barangay-cases-day')
      @slot('title', 'Cases Today')
      @slot('date', date('Y-m-d'))
      @slot('barangays', $barangays)
    @endcomponent

    @component('components.cards.barangay-cases-day')
      @slot('title', 'Cases Yesterday')
      @slot('date', Carbon\Carbon::yesterday()->toDateString())
      @slot('barangays', $barangays)
    @endcomponent

  </div>
@endsection
