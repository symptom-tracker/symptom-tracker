@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('current_page', 'Dashboard')
    @endcomponent

    <card-basic title="Admin Dashboard" class="mt-4">
      <p>
        Hi {{ Auth::user()->name }}!. You're logged in!
      </p>
      <p class="m-0">
        <a class="btn btn-primary btn-raised m-1" href="{{ route('admin.map') }}">
          Admin Map
        </a>
        <a class="btn btn-primary btn-raised m-1" href="{{ route('admin.cities.index') }}">
          Cities
        </a>
        <a class="btn btn-primary btn-raised m-1" href="{{ route('admin.barangays.index') }}">
          Barangays
        </a>
        <a class="btn btn-primary btn-raised m-1" href="{{ route('admin.cases') }}">
          Cases
        </a>
      </p>
      <template v-slot:footer>
        <a class="btn btn-primary btn-raised float-right m-1" href="{{ route('logout') }}"
          onclick="event.preventDefault();document.getElementById('logout-button').submit();">
          Logout
        </a>
        <form id="logout-button" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
        <a class="btn btn-primary btn-raised float-right m-1" href="{{ route('map') }}" target="_blank">Map</a>
        <a class="btn btn-primary btn-raised float-right m-1" href="{{ route('survey') }}" target="_blank">Survey</a>
      </template>
    </card-basic>

    <card-basic title="Statistics" class="mt-4">
      <h4>
        Reports Last Hour:
        <span class="badge badge-secondary">
          {{ $allReports->where('created_at', '>', Carbon\Carbon::now()->subHour())->count() }}
        </span>
      </h4>
      <h4>
        Reports Last 24 Hours:
        <span class="badge badge-secondary">
          {{ $allReports->where('created_at', '>', Carbon\Carbon::now()->subDay())->count() }}
        </span>
      </h4>
      <h4>
        Reports Last 7 Days:
        <span class="badge badge-secondary">
          {{ $allReports->where('created_at', '>', Carbon\Carbon::now()->subWeek())->count() }}
        </span>
      </h4>
      <h4>
        Reports Last Month:
        <span class="badge badge-secondary">
          {{ $allReports->where('created_at', '>', Carbon\Carbon::now()->subWeek())->count() }}
        </span>
      </h4>
      <h4>
        Total Reports:
        <span class="badge badge-secondary">
          {{ $allReports->count() }}
        </span>
      </h4>
    </card-basic>

    @component('components.cards.reports')
      @slot('reports', $reports)
    @endcomponent

    @component('components.cards.users')
      @slot('users', $users)
    @endcomponent

  </div>
@endsection
