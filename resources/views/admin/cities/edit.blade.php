@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($city->province->region->name, route('admin.regions.show', ['region' => $city->province->region->id])),
        array('Provinces', route('admin.provinces.index')),
        array($city->province->name, route('admin.provinces.show', ['province' => $city->province->id])),
        array('Cities', route('admin.cities.index')),
        array($city->name, route('admin.cities.show', ['city' => $city->id])),
      )))
      @slot('current_page', 'Edit')
    @endcomponent

    <card-basic title="Update City" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.cities.update', ['city' => $city->id]) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="alert alert-warning" role="alert">
          If the province you want isn't showing up in the choices below, add that province first in the database. Go <a href="{{ route('admin.provinces.index') }}">here</a>.
        </div>

        <div class="form-group">
          <label for="inputRegion" class="m-0">Region</label>
          <select class="form-control" id="inputRegion" name="province">
            @foreach($provinces as $province)
              @if($province->id == $city->province_id)
                <option value="{{ $province->id }}" selected>{{ $province->name }}</option>
              @else
                <option value="{{ $province->id }}">{{ $province->name }}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="inputName">City Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required value="{{ $city->name }}">
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
