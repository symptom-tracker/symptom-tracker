@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array($barangay->city->province->region->name, route('admin.regions.show', ['region' => $barangay->city->province->region->id])),
        array('Provinces', route('admin.provinces.index')),
        array($barangay->city->province->name, route('admin.provinces.show', ['province' => $barangay->city->province->id])),
        array('Cities', route('admin.cities.index')),
        array($barangay->city->name, route('admin.cities.show', ['city' => $barangay->city->id])),
        array('Barangays', route('admin.barangays.index')),
        array($barangay->name, route('admin.barangays.show', ['barangay' => $barangay->id])),
      )))
      @slot('current_page', 'Edit')
    @endcomponent

    <card-basic title="Update Barangay" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.barangays.update', ['barangay' => $barangay->id]) }}" method="POST">
        @method('PUT')
        @csrf

        <div class="alert alert-warning" role="alert">
          If the city you want isn't showing up in the choices below, add that city first in the database. Go <a href="{{ route('admin.cities.index') }}">here</a>.
        </div>

        <div class="form-group">
          <label for="inputCity" class="m-0">City</label>
          <select class="form-control" id="inputCity" name="city">
            @foreach($cities as $city)
              @if($city->id == $barangay->city_id)
                <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
              @else
                <option value="{{ $city->id }}">{{ $city->name }}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label for="inputName">Barangay Name</label>
          <input type="text" class="form-control" id="inputName" name="name" required value="{{ $barangay->name }}">
        </div>

        <div class="form-group">
          <label for="inputLat">Latitude</label>
          <input type="text" class="form-control" id="inputLat" name="lat" required value="{{ $barangay->lat }}">
        </div>

        <div class="form-group">
          <label for="inputLng">Longitude</label>
          <input type="text" class="form-control" id="inputLng" name="lng" required value="{{ $barangay->lng }}">
        </div>

        <button-submit class="float-right"/>
      </form>
    </card-basic>

  </div>
@endsection
