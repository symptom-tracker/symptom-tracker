@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Regions', route('admin.regions.index')),
        array('Provinces', route('admin.provinces.index')),
        array('Cities', route('admin.cities.index')),
      )))
      @slot('current_page', 'Barangays')
    @endcomponent

    <card-basic title="Map" class="mt-4">
      <template v-slot:image>
        <div id="map" class="w-100" style="height: 512px;"></div>
      </template>
    </card-basic>

    @component('components.cards.barangay-create')
      @slot('cities', $cities)
    @endcomponent

    @component('components.cards.barangays')
      @slot('barangays', $barangays)
    @endcomponent

    @foreach($barangays as $barangay)
      @component('components.cards.barangay-cases-dates')
        @slot('caseStatuses', $caseStatuses)
        @slot('barangay', $barangay)
      @endcomponent
    @endforeach

  </div>
@endsection

@section('scripts')
  <script>

    var markers = [];

    initMap = () => {

        // fetch reports
        getDataFromAPI('/api/barangays/').then(barangays => {

            const mapSettings = {
                // view of the philippines
                center: {
                    lat: 11,
                    lng: 123
                },
                zoom: 5,
                // disable satellite view
                mapTypeControl: false,
                // disable street view
                streetViewControl: false,
                styles: [{
                    "featureType": "administrative",
                    "elementType": "geometry",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "transit",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }
                ],
            };

            // map
            map = new google.maps.Map(document.getElementById('map'), mapSettings);

            // for each report
            barangays.forEach(barangay => {

                // check if coordinates exist
                if (barangay.lat != null && barangay.lng != null) {

                    var location = {
                        lat: parseFloat(barangay.lat),
                        lng: parseFloat(barangay.lng)
                    };

                    // default icon is green
                    var pinColor = '4caf50';
                    var color = 'green';

                    var icon = {
                        url: 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld= |' + pinColor,
                        size: new google.maps.Size(21, 34),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(4, 19),
                        scaledSize: new google.maps.Size(11, 20) // scaled size
                    }

                    var infowindow = new google.maps.InfoWindow({
                      content: barangay.name
                    });

                    var marker = new google.maps.Marker({
                        position: location,
                        map: map,
                        icon: icon,
                        color: color,
                    });
                    marker.addListener('click', function() {
                      infowindow.open(map, marker);
                    });
                    markers.push(marker);

                }
            });

            var options = {};

            markerCluster = new MarkerClusterer(null, markers, options);

            heatMapLayer = new google.maps.visualization.HeatmapLayer({
                data: heatMapData,
                map: map,
                dissipating: false,
                radius: 0.0075 * map.getZoom(),
                maxIntensity: 360,
            });

        });
    }
  </script>
  <script type="text/javascript" defer>
    // gets all reports from api
    getDataFromAPI = async (url) => {
      const baseURL = window.location.origin;
      const response = await fetch(baseURL + url);
      const json = await response.json();
      return json.data;
    }

    doesMapExist = () => {
      if (document.getElementById("map")) {
        initMap();
      }
      else {
        // TODO: sometimes the google maps api will call initMap() when 'map' hasn't loaded yet and it returns an error
        setTimeout(function () {
          doesMapExist();
        }, 1000);
      }
    }
  </script>
  <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('settings.google_maps_api') }}&libraries=visualization&callback=doesMapExist"></script>
@endsection
