@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Case Statuses')
    @endcomponent

    <card-basic title="Add Case Status" class="mt-4">
      @include('components.errors')
      <form action="{{ route('admin.case-statuses.store') }}" method="POST">
        @csrf
        <div class="form-group">
          <label for="inputName">Case Status</label>
          <input type="text" class="form-control" id="inputName" name="name">
        </div>
        <button-submit class="float-right"/>
      </form>
    </card-basic>

    <card-basic title="Case Statuses" class="mt-4">
      <template v-slot:image>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <td>Name</td>
                <td>Created</td>
                <td>Status</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              @foreach($caseStatuses as $caseStatus)
              <tr>
                <td>
                  {{ $caseStatus->name }}
                </td>
                <td>
                  {{ $caseStatus->created_at ? $caseStatus->created_at->diffForHumans() : '' }}
                </td>
                <td>
                  @if(isset($caseStatus->deactivated_at))
                    <span class="badge badge-warning">Deactivated</span>
                  @endif
                </td>
                <td>
                  <options-dropdown icon="more_horiz">
                    <a class="dropdown-item" href="{{ route('admin.case-statuses.edit', ['case_status' => $caseStatus->id]) }}">
                      Edit
                    </a>
                    @if(isset($caseStatus->deactivated_at))
                      <button class="dropdown-item text-success" data-toggle="modal" data-target="#confirmationModal"
                        data-action="{{ route('admin.case-statuses.activate', ['case_status' => $caseStatus->id]) }}"
                        data-method="PUT">
                        Activate
                      </button>
                    @else
                      <button class="dropdown-item text-danger" data-toggle="modal" data-target="#confirmationModal"
                        data-action="{{ route('admin.case-statuses.deactivate', ['case_status' => $caseStatus->id]) }}"
                        data-method="PUT">
                        Deactivate
                      </button>
                    @endif
                    <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                      data-action="{{ route('admin.case-statuses.destroy', ['case_status' => $caseStatus->id]) }}">
                      Delete
                    </button>
                  </options-dropdown>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </tr>
      </template>
    </card-basic>

  </div>
@endsection
