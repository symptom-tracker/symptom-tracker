@extends('layouts.empty.index')

@section('styles')
@endsection

@section('content')
  <div class="d-flex bg-green h-100">
    <div class="container h-100">

      <div class="d-flex flex-column justify-content-center align-items-center" style="height: 80%;">

        <img class="img-fluid" src="{{ asset('images/logos/logo_big_white.png') }}" style="height: 64px; width: auto;"/>

        <div class="bg-white w-100 p-4 mt-4 text-center rounded shadow">
          <h4 class="font-gotham-bold my-2 spaced-letters text-aqua text-uppercase">
            {{ __('Select language') }}
          </h4>
          <div class="d-flex flex-wrap mt-4 justify-content-center align-items-center">
            <a class="btn btn-primary p-2 m-2" href="/lang/en">English</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/fil">Filipino</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/ceb">Cebuano/Bisaya</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/hil">Hiligaynon</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/bik">Bikol</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/ilo">Ilocano</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/war">Waray</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/cbk">Chavacano</a>
            <a class="btn btn-primary p-2 m-2" href="/lang/ko">한국어</a>
            <a class="btn btn-primary-outlined text-primary p-2 m-2" href="https://forms.gle/t1kw5vYMyNWgqBwi7" target="_blank">Other</a>
          </div>
          <div class="alert alert-info mt-4" role="alert">
            <p class="m-0">
              We're still working on translating the entire site. If you see a mistake with any of our translations do message us here 😊
            </p>
            <p class="m-0">
              <a href="https://m.me/SymptomTracker" target="_blank">
                m.me/SymptomTracker
              </a>
            </p>
            <p class="m-0">
              We're also looking for volunteers to help translate the page
            </p>
          </div>
        </div>

      </div>

    </div>
  </div>
@endsection

@section('scripts')
@endsection
