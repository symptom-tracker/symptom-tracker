@extends('layouts.app.index')

@section('styles')
@endsection

@section('content')
  <div class="container-fluid">
    <div class="row">

      <div class="col-12 col-lg-8">
        <card-basic class="rounded">
          <template v-slot:image>
            <div id="map" class="w-100 rounded" style="height: 600px;"></div>
          </template>
        </card-basic>
      </div>

      <div class="col-12 col-lg-4">

        <card-basic class="border-0 rounded shadow mt-4 mt-lg-0">
          <div class="d-flex justify-content-center flex-wrap">
            <span class="d-flex align-items-center">
              From Biñan, Laguna?
            </span>
            <a href="{{ route('biñan') }}" class="btn-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center text-nowrap">
            {{ __('Go to Biñan Dashboard') }}
            </a>
          </div>
        </card-basic>

        <div class="bg-white w-100 p-2 text-center rounded shadow mt-4">
          <h4 class="font-gotham-bold my-2 spaced-letters text-aqua">{{ __('FILTER') }}</h4>
        </div>

        <card-basic class="mt-4 border-0 rounded shadow">

          @foreach(App\Symptom::all() as $symptom)
            <div class="custom-control custom-switch mt-2">
              <input
                id="symptomSwitch{{ $symptom->id }}"
                class="custom-control-input"
                type="checkbox"
                onchange="symptomSwitch(this.checked, {{ $symptom->id }})" checked>
              <label class="custom-control-label" for="symptomSwitch{{ $symptom->id }}">
                @if($symptom->name == 'Fever')
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cf44336%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Red Pin">
                @else
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cffee58%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Yellow Pin">
                @endif
                {{ __($symptom->name) }}
              </label>
            </div>
          @endforeach

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="greenMarkerSwitch" checked onchange="greenMarkerSwitch(this.checked)">
            <label class="custom-control-label" for="greenMarkerSwitch">
              <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5C4caf50%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Green Pin">
              {{ __('No symptoms') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="heatMapSwitch" onchange="heatMapSwitch(this.checked)">
            <label class="custom-control-label" for="heatMapSwitch">
              {{ __('Heatmap') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="markerClusterSwitch" onchange="markerClusterSwitch(this.checked)">
            <label class="custom-control-label" for="markerClusterSwitch">
              {{ __('Cluster markers') }}
            </label>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateFrom" class="col-sm-3 col-form-label">From</label>
            <div class="col-sm-9">
              <input type="date" min='2020-03-26' max='2020-03-26' class="form-control" id="inputDateFrom" onchange="refreshDate()">
            </div>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateTo" class="col-sm-3 col-form-label">To</label>
            <div class="col-sm-9">
              <input type="date" min='2020-03-26' max='2020-03-26' class="form-control" id="inputDateTo" onchange="refreshDate()">
            </div>
          </div>

        </card-basic>

        <card-basic class="mt-4 rounded shadow border-0">
          <div class="alert alert-info text-center" role="alert">
            {{ __('The location of the markers do not correspond to the very exact household the user reported from.') }}
            {{ __('We make sure to only get the general location of your area by randomizing your location coordinates by a bit when you submit the form') }}
          </div>

          <div class="d-flex justify-content-center w-100 mt-4">
            <a href="{{ route('survey') }}" class="btn-green mx-2 p-3 w-100 rounded font-gotham-bold text-uppercase text-center">
            {{ __('Answer for another person') }}
            </a>
            <button onclick="share()" type="button" class="btn btn-green-outlined text-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center">
            {{ __('Share') }}
            </button>
          </div>
        </card-basic>

      </div>
    </div>

  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="{{ asset('js/map.js') }}" defer></script>
  <script type="text/javascript" defer>
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }

    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("inputDateTo").setAttribute("max", today);
    document.getElementById("inputDateFrom").setAttribute("max", today);

    // gets all reports from api
    getDataFromAPI = async (url) => {
      const baseURL = window.location.origin;
      const response = await fetch(baseURL + url);
      const json = await response.json();
      return json.data;
    }

    doesMapExist = () => {
      if (document.getElementById("map")) {
        initMap();
      }
      else {
        // TODO: sometimes the google maps api will call initMap() when 'map' hasn't loaded yet and it returns an error
        setTimeout(function () {
          doesMapExist();
        }, 1000);
      }
    }
  </script>
  <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('settings.google_maps_api') }}&libraries=visualization&callback=doesMapExist"></script>
@endsection
