<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>

  @include('components.google-analytics')
  @include('components.meta')
  @include('components.icons')

  {{-- Title --}}
  <title>{{ config('app.name', 'Survey Tracker') }}</title>

  {{-- CSS --}}

  {{-- App CSS --}}
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  {{-- Material Icons --}}
  {{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons" rel="stylesheet"> --}}

  @yield('styles')

</head>

<body class="h-100">

  <div id="app" class="h-100">
    @yield('content')
  </div>

  {{-- JavaScript --}}
  <script src="{{ asset('js/app.js') }}" defer></script>

  @yield('scripts')

</body>

</html>
