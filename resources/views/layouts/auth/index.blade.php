<!doctype html>
<html class="h-100" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

  @include('components.google-analytics')

  {{-- CSRF Token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {{-- Meta data tags --}}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0">

  {{-- For Admin Pages --}}
  <meta name="robots" content="noindex, nofollow" />

  {{-- Title --}}
  <title>{{ config('app.name', 'Survey Tracker') }}</title>

  @include('components.icons')

  {{-- Material Bootstrap CSS --}}
  <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

  {{-- Material Icons --}}
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  {{-- JQuery UI --}}
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  {{-- CSS --}}
  <link href="{{ asset('css/app-md.css') }}" rel="stylesheet">

  @yield('styles')

</head>

<body class="h-100">

  <div id="app" class="h-100">
    @yield('content')
  </div>

  @include('layouts.admin.javascript')

</body>

</html>
