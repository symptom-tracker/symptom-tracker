<div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
  <header class="bmd-layout-header bg-dark">
    <div class="navbar navbar-light bg-faded">
      <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s2">
        <span class="sr-only">Toggle drawer</span>
        <i class="material-icons text-white">menu</i>
      </button>
      <ul class="nav navbar-nav">
        <li class="nav-item text-white">{{ config('app.name', 'SymptomTracker') }}</li>
      </ul>
      <ul class="nav navbar-nav invisible">
        <li class="nav-item text-white">{{ config('app.name', 'SymptomTracker') }}</li>
      </ul>
    </div>
  </header>
  <div id="dw-s2" class="bmd-layout-drawer bg-faded vh-100">
    <header class="bg-dark">
      <a class="navbar-brand text-white">{{ config('app.name', 'SymptomTracker') }}</a>
    </header>
    <ul class="list-group">
      <a href="{{ route('admin.dashboard') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">home</i>
        Dashboard
      </a>
      <a href="{{ route('admin.profile') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">account_circle</i>
        Profile
      </a>
      <a href="{{ route('admin.map') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">map</i>
        Map
      </a>
      <a href="{{ route('admin.users.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">people</i>
        Users
      </a>
      <a href="{{ route('admin.symptoms.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">people</i>
        Symptoms
      </a>
      <a href="{{ route('admin.cases') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">import_contacts</i>
        Cases
      </a>
      <a href="{{ route('admin.case-statuses.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">import_contacts</i>
        Case Statuses
      </a>
      <a href="{{ route('admin.regions.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">account_balance</i>
        Regions
      </a>
      <a href="{{ route('admin.provinces.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">account_balance</i>
        Provinces
      </a>
      <a href="{{ route('admin.cities.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">account_balance</i>
        Cities
      </a>
      <a href="{{ route('admin.barangays.index') }}" class="list-group-item">
        <i class="material-icons material-icons-outlined mr-4">account_balance</i>
        Barangays
      </a>
      <a class="list-group-item" href="{{ route('logout') }}"
        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
        <i class="material-icons material-icons-outlined mr-4">exit_to_app</i>
        Logout
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
      </form>
    </ul>
  </div>

  <main class="bmd-layout-content h-100">
    @include('components.alerts')
    @yield('content')
    <footer class="mt-4">
      <div class="text-center text-muted row py-4">
        <span class="mx-auto">
          &copy; {{ now()->year }} {{ config('app.name', 'SymptomTracker') }}
        </span>
      </div>
    </footer>
  </main>

</div>
