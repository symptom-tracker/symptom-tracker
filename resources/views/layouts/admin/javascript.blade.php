{{-- JavaScript --}}

{{-- JQuery --}}
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>

{{-- Popper --}}
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js"
  integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>

{{-- Slider --}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

{{-- Moment JS --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

{{-- Date Range Picker --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

{{-- Bootstrap Material Design --}}
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

{{-- Custom Javascript --}}
<script src="{{ asset('js/app-md.js') }}" defer></script>
<script src="{{ asset('js/vue.js') }}" defer></script>
<script src="{{ asset('js/modal-functions.js') }}" defer></script>

<script>
  $(document).ready(function() {
    $('body').bootstrapMaterialDesign();
  });
</script>

@yield('scripts')
