<nav class="navbar navbar-expand-lg navbar-light shadow bg-white w-100 position-fixed scroll-transition d-none p-0" id="app-navbar" style="z-index: 999; top: 0;">
  <div class="container p-2 font-gotham">
    <a class="navbar-brand" href="{{ route('survey') }}">
      <img class="img-responsive m-auto" src="{{ asset('images/logos/logo_banner.png') }}" style="height: 30px;" alt="SymptomTracker">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-0 ml-md-auto">
        <li class="nav-item ml-0 ml-md-3">
          <a class="nav-link text-aqua" href="{{ route('map') }}">
            {{ __('Map') }}
          </a>
        </li>
        <li class="nav-item ml-0 ml-md-3">
          <a class="nav-link text-aqua" href="{{ route('faq') }}">
            {{ __('FAQs') }}
          </a>
        </li>
        <li class="nav-item ml-0 ml-md-3">
          <a class="nav-link text-aqua" href="{{ route('about-us') }}">
            {{ __('About Us') }}
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="w-100 bg-green position-absolute mt-2" style="height: 4px; bottom: 0"></div>
</nav>
