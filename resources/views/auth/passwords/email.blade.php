@extends('layouts.auth.index')

@section('content')
  <div class="bg-dark h-100">
    <div class="container h-100">
      <div class="row justify-content-center d-flex" style="height: 80%;">
        <div class="col-md-6 align-self-center">

          <div class="mt-4">
            <a href="{{ route('survey') }}">
              <h1 class="text-white text-center">
                SymptomTracker
              </h1>
            </a>
          </div>

          <card-basic title="Reset Password">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}">
              @csrf

              <div class="form-group row">
                <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>
                <div class="col-md-12">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-md-6">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Send Password Reset Link') }}
                  </button>
                </div>
              </div>
            </form>
          </card-basic>

        </div>
      </div>
    </div>
  </div>
@endsection
