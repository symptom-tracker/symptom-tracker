@extends('layouts.new.index')

@section('styles')
@endsection

@section('content')
  <div class="container-fluid mt-4">

    <div class="d-flex flex-column justify-content-center">

      <card-basic class="rounded shadow border-0 d-flex justify-content-center align-items-center">

        <div class="d-flex justify-content-center align-items-center my-2">
          <h4 class="font-gotham-bold text-aqua">{{ __('BIÑAN, LAGUNA') }}</h4>
        </div>

        <div class="d-flex justify-content-center">
          <a href="{{ route('survey') }}" class="btn-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center">
          {{ __('Answer the survey') }}
          </a>
          <button onclick="share()" type="button" class="btn btn-green-outlined text-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center">
          {{ __('Share') }}
          </button>
        </div>

      </card-basic>

    </div>

    <div class="row mt-4">

      <div class="col-12 col-lg-8">
        <card-basic class="rounded">
          <template v-slot:image>
            <div id="map" class="w-100 rounded" style="height: 600px;"></div>
          </template>
        </card-basic>
      </div>

      <div class="col-12 col-lg-4">

        <div class="bg-white w-100 p-2 text-center rounded shadow mt-4 mt-lg-0">
          <h4 class="font-gotham-bold my-2 spaced-letters text-aqua">{{ __('FILTER') }}</h4>
        </div>

        <card-basic class="mt-4 border-0 rounded shadow">

          @foreach(App\Symptom::all() as $symptom)
            <div class="custom-control custom-switch mt-2">
              <input
                id="symptomSwitch{{ $symptom->id }}"
                class="custom-control-input"
                type="checkbox"
                onchange="symptomSwitch(this.checked, {{ $symptom->id }})" checked>
              <label class="custom-control-label" for="symptomSwitch{{ $symptom->id }}">
                @if($symptom->name == 'Fever')
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cf44336%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Red Pin">
                @else
                  <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5Cffee58%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Yellow Pin">
                @endif
                {{ __($symptom->name) }}
              </label>
            </div>
          @endforeach

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="greenMarkerSwitch" checked onchange="greenMarkerSwitch(this.checked)">
            <label class="custom-control-label" for="greenMarkerSwitch">
              <img src="https://chart.apis.google.com/chart?cht=d&chdp=mapsapi&chl=pin%27i%5Chv%27a%5C]h%5C]o%5C4caf50%27fC%5CLauto%27f%5C&ext=.png" class="img-fluid" alt="Green Pin">
              {{ __('No symptoms') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="heatMapSwitch" onchange="heatMapSwitch(this.checked)">
            <label class="custom-control-label" for="heatMapSwitch">
              {{ __('Heatmap') }}
            </label>
          </div>

          <div class="custom-control custom-switch mt-2">
            <input type="checkbox" class="custom-control-input" id="markerClusterSwitch" onchange="markerClusterSwitch(this.checked)">
            <label class="custom-control-label" for="markerClusterSwitch">
              {{ __('Cluster markers') }}
            </label>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateFrom" class="col-sm-3 col-form-label">From</label>
            <div class="col-sm-9">
              <input type="date" min='2020-03-26' max='2020-03-26' class="form-control" id="inputDateFrom" onchange="refreshDate()">
            </div>
          </div>

          <div class="form-group row mt-2 mb-0">
            <label for="inputDateTo" class="col-sm-3 col-form-label">To</label>
            <div class="col-sm-9">
              <input type="date" min='2020-03-26' max='2020-03-26' class="form-control" id="inputDateTo" onchange="refreshDate()">
            </div>
          </div>

        </card-basic>

      </div>

      <div class="col">
        <card-basic class="mt-4 rounded shadow border-0">
          <div class="alert alert-info text-center" role="alert">
            {{ __('The location of the markers do not correspond to the very exact household the user reported from.') }}
            {{ __('We make sure to only get the general location of your area by randomizing your location coordinates by a bit when you submit the form') }}
          </div>

          <div class="d-flex justify-content-center w-100 mt-4">
            <a href="{{ route('survey') }}" class="btn-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center">
            {{ __('Answer the survey') }}
            </a>
            <button onclick="share()" type="button" class="btn btn-green-outlined text-green mx-2 p-3 rounded font-gotham-bold text-uppercase text-center">
            {{ __('Share') }}
            </button>
          </div>
        </card-basic>
      </div>

    </div>

    <card-basic class="mt-4 rounded shadow border-0">
      <div class="d-flex flex-column align-items-center">

          <div class="">
            <b>Mayor Arman Dimaguila</b>
            <div>
              Facebook page: https://www.facebook.com/waldimaguila
            </div>
          </div>

          <div class="mt-4">
            <b>Binan City</b>
            <div>
              Information Office: +63495135028
            </div>
            <div>
              Facebook page: https://www.facebook.com/CIOBinan/
            </div>
          </div>

          <div class="mt-4">
            <b>City Health Office 1</b>
            <div>
              Number: +63495135028
            </div>
            <div>
              Facebook page: https://www.facebook.com/CHOBinanLaguna/
            </div>
          </div>

          <div class="mt-4">
            <b>City Health Office 2</b>
            <div>
              Number: +63495117918
            </div>
            <div>
              Facebook page: https://www.facebook.com/CHOIIBinan/
            </div>
          </div>

          <div class="mt-4">
            <b>Ospital ng Binan</b>
            <div>
              Number: +63495114119
            </div>
            <div>
              Facebook page: https://www.facebook.com/OsBinOfficial/
            </div>
          </div>

      </div>
    </card-basic>

    {{-- Tableau Card for Biñan --}}
    <card-basic class="mt-4 rounded shadow border-0">
      <div class="tableauPlaceholder" id="viz1588086083318">
        <noscript>
          <a href="#">
            <img alt="" src="https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Bi&#47;BianCityCovid19Dashboard_15873861726130&#47;Dashboard1&#47;1_rss.png" style="border: none" />
          </a>
        </noscript>
        <object class="tableauViz" style="display:none; width: 100%; height: 1200px;">
          <param name="host_url" value="https%3A%2F%2Fpublic.tableau.com%2F" />
          <param name="embed_code_version" value="3" />
          <param name="site_root" value="" />
          <param name="name" value="BianCityCovid19Dashboard_15873861726130&#47;Dashboard1" />
          <param name="tabs" value="no" />
          <param name="toolbar" value="yes" />
          <param name="static_image" value="https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Bi&#47;BianCityCovid19Dashboard_15873861726130&#47;Dashboard1&#47;1.png" />
          <param name="animate_transition" value="yes" />
          <param name="display_static_image" value="yes" />
          <param name="display_spinner" value="yes" />
          <param name="display_overlay" value="yes" />
          <param name="display_count" value="yes" />
          <param name="filter" value="publish=yes" />
        </object>
      </div>
    </card-basic>

  </div>
@endsection

@section('scripts')
  <script src="https://public.tableau.com/javascripts/api/viz_v1.js">
  </script>
  <script type='text/javascript'>
    var divElement = document.getElementById('viz1588086083318');
    // var vizElement = divElement.getElementsByTagName('object')[0];
    // if ( divElement.offsetWidth > 800 ) {
    //   vizElement.style.width='1100px';
    //   vizElement.style.height='1127px';}
    // else if ( divElement.offsetWidth > 500 ) {
    //   vizElement.style.width='1100px';
    //   vizElement.style.height='1200px';}
    // else {
    //   vizElement.style.width='100%';
    //   vizElement.style.height='1200px';
    // }
  </script>
  <script type="text/javascript" src="{{ asset('js/biñan.js') }}" defer></script>
  <script type="text/javascript" defer>
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }

    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("inputDateTo").setAttribute("max", today);
    document.getElementById("inputDateFrom").setAttribute("max", today);

    // gets all reports from api
    getDataFromAPI = async (url) => {
      const baseURL = window.location.origin;
      const response = await fetch(baseURL + url);
      const json = await response.json();
      return json.data;
    }

    doesMapExist = () => {
      if (document.getElementById("map")) {
        initMap();
      }
      else {
        // TODO: sometimes the google maps api will call initMap() when 'map' hasn't loaded yet and it returns an error
        setTimeout(function () {
          doesMapExist();
        }, 1000);
      }
    }
  </script>
  <script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('settings.google_maps_api') }}&libraries=visualization&callback=doesMapExist"></script>
@endsection
