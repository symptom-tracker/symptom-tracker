<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-2">
      <div class="modal-header py-4 row justify-content-center">
        <h5 class="modal-title">
          Are you sure?
        </h5>
      </div>
      <div class="modal-footer">
        <form method="POST">
          @method('PUT')
          @csrf
          <button type="button" class="btn btn-link text-decoration-none" data-dismiss="modal">Cancel</button>
          <button-submit text="Yes" class="btn-success"/>
        </form>
      </div>
    </div>
  </div>
</div>
