@if(session('status') || session('primary') || session('success') || session('warning') || session('danger'))
  <div class="container mt-4 mb-2">

    {{-- status alerts --}}
    @if(session('status'))
    <div class="alert alert-success alert-dismissible fade show mb-0">
      {{ session('status') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    {{-- primary alerts --}}
    @if(session('primary'))
      <div class="alert alert-primary alert-dismissible fade show mb-0">
        {{ session('primary') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    {{-- success alerts --}}
    @if(session('success'))
      <div class="alert alert-success alert-dismissible fade show mb-0">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    {{-- warning alerts --}}
    @if(session('warning'))
      <div class="alert alert-warning alert-dismissible fade show mb-0">
        {{ session('warning') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    {{-- danger alerts --}}
    @if(session('danger'))
      <div class="alert alert-danger alert-dismissible fade show mb-0">
        {{ session('danger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif

  </div>
@endif
