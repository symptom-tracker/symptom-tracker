<card-basic title="Filter" class="mt-4">
  <form method="GET" action="{{ route('admin.reports.index') }}">
    <div class="row">
      @csrf
      <div class="col-12 col-lg-6">
        <h5>Information</h5>
          <div class="form-group mt-2">
            <div class="switch">
              <label>
                <input id="symptomNumSelect" type="checkbox" name="symptom_num_select"
                @if (Request::get('symptom_num_select') == 'on')
                  checked
                @endif>
                Number of symptoms
              </label>
              <input type="number" class="form-control" name="symptom_num" id="symptomNumInput" min="1"
              value="{{ Request::get('symptom_num') ?? 1 }}">
            </div>

          </div>
          <div class="switch">
            <label>
              <input id="ageSelect" type="checkbox" name="age_select"
              @if (Request::get('age_select') == 'on')
                checked
              @endif>
              Age
            </label>
          </div>
            <div class="text-center">
              <div class="my-4 px-4">
                <input type="text" id="minAge" name="min_age" readonly class="text-center border-0 bg-transparent" style="width:30%">
                <label class="bmd-label-static">to</label>
                <input type="text" id="maxAge" name="max_age" readonly class="text-center border-0 bg-transparent" style="width:30%">
              </div>
            </div>

            <div class="mx-4" id="slider-range">
            </div>
          <div class="form-group mt-2">
            <div class="switch">
              <label>
                <input id="sexSelect" type="checkbox" name="sex_select"
                @if (Request::get('sex_select') == 'on')
                  checked
                @endif>
                Gender
              </label>
            </div>
            <select name="sex" class="form-control"id="sexInput"
              @if (Request::get('sex_select') != 'on')
                disabled
              @endif>
              <option
                @if (Request::get('sex') == null)
                    selected="selected"
                @endif>
              </option>
              <option value="1"
                @if (Request::get('sex') == 1)
                    selected="selected"
                @endif>Male
              </option>
              <option value="2"
                @if (Request::get('sex') == 2)
                    selected="selected"
                @endif>Female
              </option>
            </select>
          </div>
          <div class="form-group mt-2">
            <div class="d-inline-flex">
              <label class="mr-2">Date Range</label>
              <label id="dateLabel"></label>
            </div>
            <input type="text" id="date-range" class="form-control"
            @if(Request::get('min_date') != null)
              value="{{ date('m/d/Y', strtotime(Request::get('min_date')))}} - {{ date('m/d/Y', strtotime(Request::get('max_date')))}}"
            @endif/>
            <input type="hidden" id="minDate" name="min_date"
            @if(Request::get('min_date') != null)
              value="{{ date('Y-m-d', strtotime(Request::get('min_date'))) }}"
            @else
              value="{{ now()->format('Y-m-d') }}"
            @endif>
            <input type="hidden" id="maxDate" name="max_date"
            @if(Request::get('max_date') != null)
              value="{{ date('Y-m-d', strtotime(Request::get('max_date'))) }}"
            @else
              value="{{ now()->format('Y-m-d') }}"
            @endif>
          </div>
      </div>
      <div class="col-12 col-lg-6">
        <h5> Symptoms </h5>
        @foreach($symptoms as $symptom )
          <div class="checkbox col">
            <label>
              <input type="checkbox" value="{{ $symptom['id'] }}" name="symptom_names[]"
              @if(Request::get('symptom_names') != null && in_array($symptom->id, Request::get('symptom_names')))
                checked
              @endif>
              {{ $symptom['name'] }}
            </label>
          </div>
        @endforeach
      </div>
    </div>
    <div class="d-flex flex-row-reverse">
      <button class="btn btn-raised btn-primary" type="submit">Filter</button>
    </div>
  </form>
</card-basic>
