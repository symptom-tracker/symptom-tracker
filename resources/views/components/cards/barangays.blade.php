<card-basic title="Barangays" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Name</td>
            <td>Lat</td>
            <td>Lng</td>
            <td>Region</td>
            <td>Created</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          @foreach($barangays as $barangay)
          <tr>
            <td>
              <a href="{{ route('admin.barangays.show', ['barangay' => $barangay->id]) }}">
                {{ $barangay->name }}
              </a>
            </td>
            <td>
              {{ $barangay->lat }}
            </td>
            <td>
              {{ $barangay->lng }}
            </td>
            <td>
              {{ $barangay->city ? $barangay->city->name : '' }}
            </td>
            <td>
              {{ $barangay->created_at ? $barangay->created_at->diffForHumans() : '' }}
            </td>
            <td>
              <options-dropdown icon="more_horiz">
                <a class="dropdown-item" href="{{ route('admin.barangays.edit', ['barangay' => $barangay->id]) }}">
                  Edit
                </a>
                <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                  data-action="{{ route('admin.barangays.destroy', ['barangay' => $barangay->id]) }}">
                  Delete
                </button>
              </options-dropdown>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
