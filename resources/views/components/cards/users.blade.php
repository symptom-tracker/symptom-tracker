<card-basic title="Users" href="{{ route('admin.users.index') }}" class="mt-4">
  <template v-slot:options>
    <options-dropdown>
      <a class="dropdown-item" href="{{ route('admin.users.create') }}">
        <i class="material-icons material-icons-outlined mr-3">person_add</i>
        Add User
      </a>
    </options-dropdown>
  </template>
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Joined</td>
            <td>Last Login</td>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>
              <a href="{{ route('admin.users.show', ['user' => $user->id]) }}">
                {{ $user->name }}
              </a>
            </td>
            <td>
              {{ $user->email }}
            </td>
            <td>
              {{ $user->created_at ? $user->created_at->diffForHumans() : '' }}
            </td>
            <td>
              {{ $user->lastLogin ? $user->lastLogin->created_at->diffForHumans() : '' }}
            </td>
            <td>
              @if(Auth::user()->id != $user->id && strcmp($user->email, "cyrus_vatandoostkakhki@dlsu.edu.ph") != 0)
                <options-dropdown icon="more_horiz">
                  <button class="dropdown-item text-danger" data-toggle="modal" data-target="#deleteConfirmationModal"
                    data-action="{{ route('admin.users.destroy', ['user' => $user->id]) }}">
                    Delete
                  </button>
                </options-dropdown>
              @endif
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
