<card-basic title="{{ $barangay->name }}" href="{{ route('admin.barangays.show', ['barangay' => $barangay->id]) }}" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <td>Date</td>
            @foreach(App\CaseStatus::where('deactivated_at', null)->get() as $caseStatus)
              <td>{{ $caseStatus->name }}</td>
            @endforeach
          </tr>
        </thead>
        <tbody>
          @foreach($barangay->cases->sortByDesc('date')->groupBy('date') as $dateCases)
            <tr>
              <td>
                {{ $dateCases->first()->date }}
              </td>
              @foreach(App\CaseStatus::where('deactivated_at', null)->get() as $caseStatus)
                <td>
                  {{ $dateCases->where('case_status_id', $caseStatus->id)->where('date', $dateCases->first()->date)->first() ? $dateCases->where('case_status_id', $caseStatus->id)->where('date', $dateCases->first()->date)->first()->number : '' }}
                </td>
              @endforeach
            </tr>
          @endforeach
        </tbody>
      </table>
    </tr>
  </template>
</card-basic>
