{{--
@component('components.breadcrumb')
  @slot('pages', array(
    array('Dashboard', route('admin.dashboard')),
    array('Businesses', route('admin.businesses.index')),
    array($branch->business->name, route('admin.businesses.show', ['business' => $branch->business->id])),
    array('Branches', route('admin.businesses.branches.index', ['business' => $branch->business->id])),
  )))
  @slot('current_page', $branch->name)
@endcomponent
--}}

<div class="mt-4">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb flex-nowrap m-0 overflow-auto">
      @if(isset($pages))
        @foreach($pages as $page)
          <li class="breadcrumb-item text-nowrap">
            <a href="{{ $page[1] }}">
              {{ $page[0] }}
            </a>
          </li>
        @endforeach
      @endif
      <li class="breadcrumb-item active text-nowrap pr-3" aria-current="page">
        {{ $current_page }}
      </li>
    </ol>
  </nav>
</div>
